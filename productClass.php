<?php

abstract class Product 
{ protected $title;
	protected $price;
	protected $discount;
	protected $count;
	function __construct($title = "", $price = 0 )
	{
		$this->title = $title;
		$this->price = $price;
		$this->discount = 0;
		$this->count = 1;
	}
	public function getTitle()
	{
		return $this->title;
	}
	abstract public function getPrice();
	
	public function getTotal()
	{
		return   $this->getPrice()*$this->count;
	}
	public function setCount($count)
	{
		$this->count = $count;
	}
	public function getCount($count)
	{
		return $this->count ;
	}
}	

trait deliver 
{
	public function getDeliver()
	{
	  if ($this->discount == 0) 
		{
			return  250;
		} else
		{
			return 300;
		}
	}
}

class Dress extends Product
{ protected $size; 
	protected $color; 
	public function __construct($title = "",$price =0,$size,$color)
	{
		parent::__construct($title, $price);  
		$this->size = $size;
		$this->color = $color;   
		$this->discount = 10;
	}
	public function getPrice()
	{
		return $this->price*(1-$this->discount/100); 
	}	
	public function showPriceList()
	{
		return "платье " . $this->title . "цвет ". $this->color . " цена со скидкой " . $this->getPrice() . " доставка " . $this->getDeliver();
	}
	public function getTotal()
	{ 
		parent::getTotal();  
	}
	use deliver;
}


class Cupboard extends Product
{ protected $length;
	protected $height;
	protected $depth; 
	protected $color;
	public function __construct($title = "",$price =0,$length, $height, $depth, $color)
	{
		parent::__construct($title, $price);  
		$this->length = $length;
		$this->height = $height;
		$this->depth = $depth;
		$this->color = $color;   
		$this->discount = 10;
	}
	public function getPrice()
	{
		return $this->price*(1-$this->discount/100); 
	}	
	public function showPriceList()
	{
		return "Шкаф " . $this->title . " цвет ". $this->color . " Габариты (" . $this->height . "x" .$this->length . "x" . $this->depth . ") Цена со скидкой". $this->getPrice() . " доставка " . $this->getDeliver();
	}
	public function getTotal()
	{ 
		parent::getTotal();  
	}
	use deliver;
}

class Pasta extends Product
{
	protected $typePack; 
	protected $weightPack; 
	
	public function __construct($title ,$price,$typePack ="",$weightPack=0)
	{
		parent::__construct($title, $price);  
		$this->typePack = $typePack;
		$this->weightPack = $weightPack;   
		$this->discount = 10;
	}
	public function showPriceList()
	{
		return "макароны " . $this->title . " упаковка ". $this->typePack . " вес упаковки " . $this->weightPack . " цена  " . $this->getPrice() . " доставка " . $this->getDeliver();
	}
	public function setCount($count)
	{ 
		parent::setCount($count);  
		if  ($this->count*$this->weightPack > 10) 
		{
			$this->discount = 10;	
		} else
		{
			$this->discount = 0;	
		}
	} 
	public function getPrice()
	{
		return $this->price*(1-$this->discount/100); 
	}	
	use deliver;
}


$dress_1 = new Dress( "Cарафан",300,44,"красный");
print_r($dress_1->showPriceList());echo "<br>";
$cupboard_1 = new Cupboard( "Шкаф для посуды",15000,2200,800,400,"Венге");
print_r($cupboard_1->showPriceList());echo "<br>";

$pasta_1 = new Pasta( "Спагетти",50,"коробка",0.5);
$pasta_1->setCount(25);
print_r($pasta_1->showPriceList());echo "<br>";
$pasta_1->setCount(10);
print_r($pasta_1->showPriceList());echo "<br>";

